﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios, jthorborg.com]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt;
using Blunt.Sound;
using Blunt.Synthesis;
using Blunt.Synthesis.Filters;

using SystemFloat = System.Single;

public class FilterEffect : MonoBehaviour
{
	public float cutoff;
	public float Q;
	public float hpMix, lpMix, bpMix;

	public bool isEnabled;
	public float depth;

	private Filter effect;

	void Awake()
	{
        onAudioLoad();
	}

	void onAudioLoad()
	{
		effect = new Filter();
	}

	public void applyChanges()
	{
		effect.svfs[0].design(cutoff, Q);
		effect.svfs[1].design(cutoff, Q);

		effect.hpMix = Mathf.Clamp01(hpMix);
		effect.bpMix = Mathf.Clamp01(bpMix);
		effect.lpMix = Mathf.Clamp01(lpMix);

		effect.depth = Mathf.Clamp01(depth);
	}

	public void OnAudioFilterRead(SystemFloat[] data, int nChannels)
	{
		if(isEnabled && effect != null)
		{
			applyChanges();
			effect.process(data, data.Length / nChannels, nChannels, false);
		}

	}

	public class Filter : SoundStage
	{
		public float hpMix, bpMix, lpMix;
		public float depth;
		public SVFBiquad[] svfs = { new SVFBiquad(), new SVFBiquad() };

		public void process(SystemFloat[] data, int nSampleFrames, int channels, bool channelIsEmpty)
		{
			unchecked
			{
				// nothing to do here
				if(channelIsEmpty)
					return;

				float z1, z2, f, g, r;
				float hp, bp, lp;

				float outSample = 0.0f;

				float mix = depth, invMix = 1.0f - depth;
				float sample;
				for(int k = 0; k < channels; ++k)
				{
					// load filter coefficients
					z1 = svfs[k].z1;
					z2 = svfs[k].z2;
					f = svfs[k].f;
					g = svfs[k].g;
					r = svfs[k].r;

					for(int i = 0; i < nSampleFrames; i++)
					{
						sample = data[i * 2 + k];
                        hp = (sample - r * z1 - z2) * g;
						bp = z1 + f * hp;
						lp = z2 + f * bp;
						// update of delay-state variables
						z1 += 2 * f * hp;
						z2 += 2 * f * bp;
						// store output filter
						outSample = hp * hpMix + bp * bpMix + lp * lpMix;
						// mix out sample
						data[i * 2 + k] = outSample * mix + sample * invMix;

					}

					// store states
					svfs[k].z1 = z1;
					svfs[k].z2 = z2;

				}

			}
	
		}
	}
}
