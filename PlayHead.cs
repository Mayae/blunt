﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios, jthorborg.com]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using System;

namespace Blunt.Synthesis
{

	/// <summary>
	/// PlayHeads represents a fine-tuned, exact point in time, that is advancable by discrete amounts of time.
	/// Playheads operate using doubles, since musical divisions can be exactly represented in powers-of-two, making
	/// it possible to have many bases in a single format, along with the possibility of nearly-arbitrary precision offsets.
	/// Along with this functionality, the playhead supports getting distances to next perfect multiple of diverse divisions, making
	/// it possible to completely normalize your current position and thereby easily support random-sequencing with offsets.
	/// Also provides a convinient way to calculate the current position in one place.
	/// Playheads are always tuned according to the EnvironmentTuning.
	/// </summary>
	public class PlayHead
	{
		public struct TimeOffset
		{
			public double beats;
			public double differenceInSeconds
			{
				get
				{
					return beats / EnvironmentTuning.baseBPS;
				}
			}
			static public TimeOffset zero = new TimeOffset(0);
			public TimeOffset(double beatDifference)
			{
				beats = beatDifference;
			}

			public static TimeOffset operator + (TimeOffset t1, TimeOffset t2)
			{
				return new TimeOffset(t1.beats + t2.beats);
			}
			public static TimeOffset operator - (TimeOffset t1, TimeOffset t2)
			{
				return new TimeOffset(t1.beats - t2.beats);
			}
			public override string ToString()
			{
				return "{beats: " + beats + ", seconds: " + differenceInSeconds + "}";
			}

			public static implicit operator TimeOffset(double beats)
			{
				return new TimeOffset(beats);
			}


		}
		/// <summary>
		/// The current total number of samples since this synth was reset.
		/// </summary>
		public long nTotalSamples;
		/// <summary>
		/// EnvironmentTuning.deltaT * nTotalSamples
		/// </summary>
		public double timeInSeconds;
		/// <summary>
		/// nTotalSamples / samplesPerBeat.
		/// The precise timing in beats.
		/// </summary>
		public double preciseTiming;
		/// <summary>
		/// The size of the next batch of samples.
		/// </summary>
		public int samples;
		/// <summary>
		/// The current number of surpassed bars
		/// </summary>
		public int bars;
		/// <summary>
		/// The current number of beats.
		/// For 4/4, this value will vary between 0 and 3
		/// </summary>
		public int beats;
		/// <summary>
		/// The 16th division of the beat.
		/// </summary>
		public int divs;
		/// <summary>
		/// The fraction of the beat we're currently on.
		/// </summary>
		public float fractionateBeat;
		/// <summary>
		/// The lowest rational fraction that can be expressed as a timing difference.
		/// </summary>
		public float quantization;

		void reset()
		{
			timeInSeconds = fractionateBeat = quantization = nTotalSamples = samples = bars = beats = 0;
		}

		public PlayHead()
		{
			reset();
		}
		/// <summary>
		/// Advances the playhead by an amount of samples, updating all its state.
		/// </summary>
		/// <param name="nSamples"></param>
		public void advance(int nSamples)
		{
			nTotalSamples += nSamples;
			samples = nSamples;
			quantization = 1.0f / ((float)EnvironmentTuning.samplesPerBeat / nSamples);
			timeInSeconds = EnvironmentTuning.deltaT * nTotalSamples;

			preciseTiming = (double)(nTotalSamples) / EnvironmentTuning.samplesPerBeat;
			int totalBeats = (int)preciseTiming;
			// add support here for other stuff than 4/4
			bars = totalBeats >> 2;
			beats = totalBeats & 0x3;
			fractionateBeat = (float)(preciseTiming - totalBeats);
			divs = (int)(fractionateBeat * 16);
		}
		/// <summary>
		/// Returns the 'time' it takes to progress the given amount of bars, beats, divisions and / or fraction.
		/// </summary>
		/// <param name="abars"></param>
		/// <param name="abeats"></param>
		/// <param name="adivs"></param>
		/// <param name="afraction"></param>
		/// <returns></returns>
		static public TimeOffset createTimeOffset(int abars, int abeats, int adivs, float afraction)
		{
			TimeOffset ret;
			ret.beats = abars * 4 + abeats + adivs * EnvironmentTuning.reciprocalDivsPerBeat + afraction;
			return ret;
		}
		/// <summary>
		/// Gets the 'distance' in time to the next multiple of the bar number, plus an additional
		/// fractionate offset.
		/// </summary>
		/// <param name="abars"></param>
		/// <param name="abeats"></param>
		/// <param name="adivs"></param>
		/// <param name="afraction"></param>
		/// <returns></returns>
		public TimeOffset getDistanceToNextBar(int abars, int abeats, int adivs, float fraction)
		{
			TimeOffset ret;
			// we convert everything to the base of bars, so we can use integer
			// arithmetic to reduce the bar part.
			double now = preciseTiming * EnvironmentTuning.reciprocalBeatsPerBar;
			// integer part of 'now'
			int inow = (int)(now);
			double newFraction = abeats * EnvironmentTuning.reciprocalBeatsPerBar
				+ adivs * EnvironmentTuning.reciprocalDivsPerBar + fraction * EnvironmentTuning.reciprocalBeatsPerBar;
			int inewFraction = (int)(newFraction);
			abars += inewFraction;
			newFraction -= (inewFraction);
			int reducedNow = abars > 0 ? inow % abars : 0;

			double oldFraction = now - inow;

			/*if(reducedNow == 0 && newFraction == 4.0 && oldFraction == 0.0)
			{
				ret.beats = 0;
			}
			// special case; otherwise we need a modf on the output.
			else */if(reducedNow == 0 && newFraction == oldFraction)
			{
				ret.beats = newFraction - oldFraction;
			}
			else if(abars == 0 && oldFraction > newFraction)
			{
				ret.beats = (1 + newFraction) - (reducedNow + oldFraction);
			}
			else // standard case
			{
				ret.beats = (abars + newFraction) - (reducedNow + oldFraction);
			}
			ret.beats *= 4;	// (our base was bars)
			return ret;
		}
        /// <summary>
        /// Gets the 'distance' in time to the next multiple of the bar number, plus an additional
        /// fractionate offset, calculated from a parameter value instead of an playhead.
        /// playHeadTiming corrosponds to playHead.preciseTiming
        /// </summary>
        /// <param name="abars"></param>
        /// <param name="abeats"></param>
        /// <param name="adivs"></param>
        /// <param name="afraction"></param>
        /// <returns></returns>
        static public TimeOffset getDistanceToNextBar(double playHeadTiming, int abars, int abeats, int adivs, float fraction)
        {
            TimeOffset ret;
            // we convert everything to the base of bars, so we can use integer
            // arithmetic to reduce the bar part.
            double now = playHeadTiming * EnvironmentTuning.reciprocalBeatsPerBar;
            // integer part of 'now'
            int inow = (int)(now);
            double newFraction = abeats * EnvironmentTuning.reciprocalBeatsPerBar
                + adivs * EnvironmentTuning.reciprocalDivsPerBar + fraction * EnvironmentTuning.reciprocalBeatsPerBar;
            int inewFraction = (int)(newFraction);
            abars += inewFraction;
            newFraction -= (inewFraction);
            int reducedNow = abars > 0 ? inow % abars : 0;

            double oldFraction = now - inow;

            /*if(reducedNow == 0 && newFraction == 4.0 && oldFraction == 0.0)
			{
				ret.beats = 0;
			}
			// special case; otherwise we need a modf on the output.
			else */
            if (reducedNow == 0 && newFraction == oldFraction)
            {
                ret.beats = newFraction - oldFraction;
            }
            else if (abars == 0 && oldFraction > newFraction)
            {
                ret.beats = (1 + newFraction) - (reducedNow + oldFraction);
            }
            else // standard case
            {
                ret.beats = (abars + newFraction) - (reducedNow + oldFraction);
            }
            ret.beats *= 4; // (our base was bars)
            return ret;
        }
        /// <summary>
        /// Returns the time to the next perfect beat.
        /// </summary>
        /// <returns></returns>
        public TimeOffset getDistanceToNextBeat()
		{
			return Math.Ceiling(preciseTiming) - preciseTiming;
		}
		/// <summary>
		/// Returns the 'time' to the next perfect division of this beat.
		/// </summary>
		/// <returns></returns>
		public TimeOffset getDistanceToNextDivision()
		{
			return (Math.Ceiling(preciseTiming * 16) - preciseTiming * 16) / 16;
		}
		/// <summary>
		/// Returns the current playhead time as an offset (combo beat/second wrapper)
		/// </summary>
		/// <returns></returns>
		public TimeOffset getCurrentOffset()
		{
			TimeOffset ret;
			ret.beats = preciseTiming;
			return ret;
		}
		public override string ToString()
		{
			return "{bars: " + bars + ", beats: " + beats + ", division: " + divs + "(" + fractionateBeat + ")} => " + getCurrentOffset();
		}
	}
}